# CloudStorage

<p align="center">
Individual project.
</p>
<p align="center">
Faculty of Mathematics and Informatics.
</p>
<p align="center">
Cloud computing.
</p>
<p align="center">
Developed &copy; by Volodymyr Kadzhaia.
</p> 

[TOCM]

[TOC]

Contents:

  * [Overview](#overview)
  * [Product description](#product-description)
  * [Specific Requirements](#specific-requirements)
  * [Functional Requirements](#functional-requirements)
  * [Non-Functional Requirements](#non-functional-requirements)
  * [Use Cases](#use-cases)
  * [Architecture](#architecture)
  * [Contributing](#contributing)
  * [Suggested Contributions and Innovations](#suggested-contributions-and-innovations)
  * [Reference](#reference)
  * [LICENSE](#license)
  

## Overview

An android application which use different cloud systems such as Firebase, Azure, Back4App, etc.

### Purpose

The purpose of this document is to describe the implementation of the software of the Cloud Storage. This SRS document is intended to describe all the behaviors, requirements, design constraints and necessary factors of the software. This application cover the following features: 
* Running on android phones (on any android os version); 
* Choosing the cloud service which app provides;
* Upload files (.jpg);
* Make a thread and upload parallel to another cloud service;

### Scope

This application can be used in all areas that contain Internet signals such as Wi-Fi or 3G and even LTE. Shopping malls, schools, universities, business centers, airports, hospitals etc. Common features of all these structures are storage the files and promoting their stuff. If there is no Wi-Fi signal available or Internt connection, this application will display the information that there is no Internet accesss.

### Definitions, Acronyms and Abbreviations
| Terms                                     | Definitions                                                                                                                                                                                                                                                                                           |
|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Android                                   | Android is a mobile/desktop operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets.                                                                                       |
| Software Requirements Specification (SRS) | A software requirements specification is a description of a software system to be developed. It is modeled after business requirements specification.                                                                                                                                                 |
| Cloud Services                            | Cloud computing is the on-demand availability of computer system resources, especially data storage and computing power, without direct active management by the user. Large clouds often have functions distributed over multiple locations, each location being a data center.                      |
| SDK                                       | A software development kit is a collection of software development tools in one installable package. They facilitate the creation of applications by having a compiler, debugger and perhaps a software framework.                                                                                    |
| API                                       | An application programming interface is a connection between computers or between computer programs. It is a type of software interface, offering a service to other pieces of software.                                                                                                              |
| Wi-Fi                                     | Wi-Fi is the wireless technology used to connect computers, tablets, smartphones and other devices to the internet                                                                                                                                                                                    |
| 3G                                        | 3G is the third generation of wireless mobile telecommunications technology. It is the upgrade for 2.5G GPRS and 2.75G EDGE networks, for faster data transfer.                                                                                                                                       |
| LTE                                       | In telecommunications, Long-Term Evolution is a standard for wireless broadband communication for mobile devices and data terminals, based on the GSM/EDGE and UMTS/HSPA technologies. It increases the capacity and speed using a different radio interface together with core network improvements. |
| Thread                                    | A thread is a thread of execution in a program. The Java Virtual Machine allows an application to have multiple threads of execution running concurrently. Every thread has a priority. Threads with higher priority are executed in preference to threads with lower priority. Each thread may or may not also be marked as a daemon. When code running in some thread creates a new Thread object, the new thread has its priority initially set equal to the priority of the creating thread, and is a daemon thread if and only if the creating thread is a daemon. |

## Product Description

### Product Perspective

Our project allows to choose cloud system from this list and upload file  using Wi-Fi, 3G or LTE and parallel to one cloud user has option to upload it parallel to another cloud system.

The algorithm we use is a parallelism. Developing simple thread pool. After the completion of one stream, another stream is created that automatically uploads the file to another cloud system.

This system consists of two parts; one is smart mobile hardware and other one is mobile application which is running on the hardware. This application is running on the Android system.

### User Characteristics

**Wi-Fi Connection/3G/LTE**

Wi-Fi, or 3G, or LTE of android mobile must be active. Otherwise, the application cannot upload files. 

**Security Importance**

Our application’s security equals to Wi-Fi/3G/LTE signals security. So, Wi-Fi/3G/LTE signals can create a security issue for our application. We do not know Wi-Fi/3G/LTE signals security situation.

**Battery Consumption**

It is not consume too much battery power.

## Specific Requirements

### Android

We need a smartphone with the features needed to use our software. This smartphone should work with the android operating system and Android Operating System satisfies these properties:
* Min SDK version: 14
* Target SDK version: 19
* Compile SDK version: 26
* Build Tools version: 25.0.3

### Cloud Service



### Client Application Requirements

Since our project works on Android platform, users must use Android mobile phone. Minimum Android version requirement is Android 4.0-4.0.2 – Ice Cream.

## Functional Requirements

| Use Case | Requirements                                       | Priority |
|----------|----------------------------------------------------|----------|
| #1       | The user must have Internet connection             | 1        |
| #2       | The user must choose cloud service from the list   | 2        |
| #3       | The user must upload file                          | 3        |
| #4       | The user can be informed if no Internet connection | 1        |
| #5       | The user can be informed if file not uploaded      | 1        |
| #6       | The user can be informed parallel task is running  | 3        |

## Non-Functional Requirements

* Performance
Our application is using Wi-Fi triangulation algorithms for detection current position of user. Detection of Wi-Fi signals and computations of algorithms depend on user’s phone performance. 

* Security
If Wi-Fi/3G/LTE signals have security, also our application will has security. So, Wi-Fi/3G/LTE signals can create a security issue for our application. 

* Usability
User can choose cloud service and upload files with one click.

## Use Cases

Complete system use case is shown in the Figure 1.

![Use Case Diagram](/docs/1.png)

## Architecture

Each cloud service has own architecture. The information about the architecture is taken from [the official websites](#reference).

### Parallel Processing

All Android apps use a main thread to handle UI operations. Calling long-running operations from this main thread can lead to freezes and unresponsiveness. For example, if your app makes a network request from the main thread, your app's UI is frozen until it receives the network response. You can create additional background threads to handle long-running operations while the main thread continues to handle UI updates.

When an application is launched, the system creates a thread of execution for the application, called "main." This thread is very important because it is in charge of dispatching events to the appropriate user interface widgets, including drawing events. 

Threads allow you to perform several tasks at the same time without interfering with each other, which makes it possible to efficiently use system resources. Streams are used when one long-running action shouldn't interfere with other actions.

Parallel processing of application is shown in the Figure 2.

![Parallel Processing Diagram](/docs/2.png)

### Concurency Problem

Because threads run at the same time as other parts of the program, there is no way to know in which order the code will run. When the threads and main program are reading and writing the same variables, the values are unpredictable. The problems that result from this are called concurrency problems. 

To avoid concurrency problems, it is best to share as few attributes between threads as possible. If attributes need to be shared, one possible solution is to use the ```isAlive()``` method of the thread to check whether the thread has finished running before using any attributes that the thread can change.

### Sleep

sleep(): This method causes the currently executing thread to sleep for the specified number of milliseconds, subject to the precision and accuracy of system timers and schedulers.

* Based on the requirement we can make a thread to be in sleeping state for a specified period of time
* Sleep() causes the thread to definitely stop executing for a given amount of time; if no other thread or process needs to be run, the CPU will be idle (and probably enter a power saving mode).

Thread.sleep() interacts with the thread scheduler to put the current thread in wait state for specified period of time. Once the wait time is over, thread state is changed to runnable state and wait for the CPU for further execution. So the actual time that current thread sleep depends on the thread scheduler that is part of operating system.

### REST API

OkHttp perseveres when the network is troublesome: it will silently recover from common connection problems. If your service has multiple IP addresses, OkHttp will attempt alternate addresses if the first connect fails. This is necessary for IPv4+IPv6 and services hosted in redundant data centers. OkHttp supports modern TLS features (TLS 1.3, ALPN, certificate pinning). It can be configured to fall back for broad connectivity.

Using OkHttp is easy. Its request/response API is designed with fluent builders and immutability. It supports both synchronous blocking calls and async calls with callbacks.

![REST API Diagram](/docs/3.png)

[Backend server](https://gitlab.com/ladroid/vu-cloud-backend-api)

### Firebase Architecture

FCM relies on the following set of components that build, transport, and receive messages:

1. Tooling to compose or build message requests. The Notifications composer provides a GUI-based option for creating notification requests. For full automation and support for all message types, you must build message requests in a trusted server environment that supports the Firebase Admin SDK or the FCM server protocols. This environment could be Cloud Functions for Firebase, App Engine, or your own app server.

![Firebase architecture](https://firebase.google.com/docs/cloud-messaging/images/diagram-FCM.png)

2. The FCM backend, which (among other functions) accepts message requests, performs fanout of messages via topics, and generates message metadata such as the message ID.

3. A platform-level transport layer, which routes the message to the targeted device, handles message delivery, and applies platform-specific configuration where appropriate. This transport layer includes:

  * Android transport layer (ATL) for Android devices with Google Play services
  * Apple Push Notification service (APNs) for Apple devices
  * Web push protocol for web apps

4. The FCM SDK on the user’s device, where the notification is displayed or the message is handled according to the app’s foreground/background state and any relevant application logic.

### Azure

Azure Storage supports three types of blobs:

  * Block blobs store text and binary data. Block blobs are made up of blocks of data that can be managed individually. Block blobs can store up to about 190.7 TiB.
  * Append blobs are made up of blocks like block blobs, but are optimized for append operations. Append blobs are ideal for scenarios such as logging data from virtual machines.
  * Page blobs store random access files up to 8 TiB in size. Page blobs store virtual hard drive (VHD) files and serve as disks for Azure virtual machines.

![Azure Architecture](https://miro.medium.com/max/1004/0*PYSS__o2sYQBYgKd.png)

### Cloudinary

Cloudinary's media asset management service includes:

  * High performance servers behind Elastic Load Balancers that support fast upload and download rates.
  * Highly available storage that promises that your assets are always available and safe.
  * High performance media processing servers for generating your requested images and videos.
By default, assets are stored in Amazon S3 buckets that are privately accessed for writing. These can be shared buckets, dedicated buckets or private buckets.

![Cloudinary Architecture](https://res.cloudinary.com/demo/image/upload/service_architecture.jpg)

### DropBox SDK

![DropBox Architecture](https://aem.dropbox.com/cms/content/dam/dropbox/www/en-us/business/trust/dropbox_system_architecture.png)

### Back4App

Key features of Back4app:

 * **Spreadsheet like database.** Back4app is supporting a spreadsheet like database solution which will offer an easier way for developers to create, update and sync their data of applications. Even more, Back4app will let its users to import or export their JSON/CSV files with one click only by using Parse dashboard. Controlling and managing your data with this database is easier and faster.  

 * **REST and GraphQL APIs.** Back4app is offering multiple features for developers to accomplish their backend goals in the best possible way. However, Back4app is playing a vital role of converting any coded or codeless logic into an API by using SDKs and REST API. More amazingly, this feature can help users to invoke any method and inspect response of their added services in a more effective way. Not only this, back4app is also offering an opportunity of creating GraphQL APIs. GraphQL is one of the most important API to be considered in any development stack. It is generally a query language for executing server-side queries and API based on system type which couldn’t be addressed with other API options. 

 * **Live Queries.** Back4app is offering an amazing feature of live queries which can enable developers to subscribe to a certain query, synchronize and store application data in real-time in a more effective way. The server can notify its clients whenever object matching to their query will be updates, created or modified.

 * **Scalable Hosting.** In most of the cases workload of application vary on the basis of different factors. Therefore, when it comes to host an application then most of the people don’t know how much hosting services are needed for their application. However, back4app has made the things easier for people by offering scalable hosting solutions. This feature will let developers to scale their hosting services as and when needed to let users experience the best performance in a more effective way. 

### Box SDK

**Administrative tasks**

![Administrative tasks](https://developer.box.com/static/3c5e0e0bd49f47aa06f19392620a95ec/a2303/admin_tasks.webp)

Components:

  * A server or local machine running a PowerShell script
  * An identity provider with a user provisioning/deprovisioning service
  * A Service Account owned folder containing a personal folder for each user
  * A PowerShell script based on a time that monitors the event stream and creates/collaborates each user on their personal folder

**Vault Portal**

![Vault Portal](https://developer.box.com/static/a8bb13a0b69b96a85232ecda40e2c297/a2303/vault_portal.webp)

Components:

  * A custom portal allowing users to collaborate in a non-Box branded environment
  * A load balancer distributing users to a web server with the deployed portal
  * Users can login using credentials maintained in an identity provider, which are then mapped to App User information from Box within a data server.
  * Other site data is stored on the data server

**Box Skill**

![Box Skill](https://developer.box.com/static/058172302103cc61ed1027644618eef1/a2303/box_skill2.webp)

In this example, external users upload their resumes via a file request.

A Box Skill is set to monitor any upload/move/copy actions in a specific folder. When an event occurs, the file is sent to a cloud provider to be processed by any machine learning service. Once it is processed, information is saved back to the file as metadata. This metadata can then be used in another process or for future reference.

## Contributing

Interested in contributing? That's great! Here are some [Contribution Guidelines](/CONTRIBUTING.md) and the [Code of Conduct](/CODE_OF_CONDUCT.md).

## Suggested Contributions and Innovations

The following suggestions are beyond the scope of this limited thought leadership reference app. We encourage the community to contribute to this repo, or fork it and innovate!

* Generate secure keys to users.
* Apps facilitating onboarding of new Secure keys to users' accounts.
* Multi-device support and onboarding new devices.
* If you lose your device(s), you lose your Secure keys. How can recovery be done without custodians?
* Support for Universal Links and other transports.
* Increase number of cloud services.
* Uploading different types of files.

## Reference

1. https://firebase.google.com/docs/cloud-messaging/fcm-architecture
2. https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blobs-introduction
3. https://cloudinary.com/documentation/solution_overview
4. https://www.dropbox.com/en_GB/business/trust/security/architecture
5. https://blog.back4app.com/what-is-back4app/
6. https://developer.box.com/guides/getting-started/architecture-patterns/

## LICENSE

[BSD3](/LICENSE.md)