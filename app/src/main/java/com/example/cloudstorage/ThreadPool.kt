package com.example.cloudstorage

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.cloudinary.android.MediaManager
import com.cloudinary.android.callback.ErrorInfo
import com.cloudinary.android.callback.UploadCallback
import com.example.cloudstorage.utility.FileHelper
import com.parse.ParseFile
import com.parse.ParseObject

class ThreadPool {

    private val TAG = "Uploading"

    fun uploader_parse(context: Context, uri: Uri?) {
        val imageUpload = ParseObject("Uploads")
        try {
            var fileBytes = FileHelper().getByteArrayFromFile(context, uri!!)
            if(fileBytes == null) {
                Toast.makeText(context, "Error!!!", Toast.LENGTH_SHORT).show()
            } else {
                val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                fileBytes = FileHelper().reduceImageForUpload(bitmap)
                val fileName = FileHelper().getFileName(context, uri, "image")
                val file = ParseFile(fileName, fileBytes)
                imageUpload.saveEventually{
                    if(it == null) {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                        imageUpload.put("imageContent", file)
                        imageUpload.saveInBackground()
                    } else {
                        Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch(e1: Exception) {
            Toast.makeText(context, e1.message, Toast.LENGTH_SHORT).show()
        }
    }

    fun uploadToCloudinary(imagePath: Uri) {
        MediaManager.get().upload(imagePath).callback(object : UploadCallback {
            override fun onStart(requestId: String) {
                Log.e(TAG, "onStart: "+"started")
            }

            override fun onProgress(requestId: String, bytes: Long, totalBytes: Long) {
                Log.e(TAG, "onStart: "+"uploading")
            }

            override fun onSuccess(requestId: String, resultData: Map<*, *>) {
                Log.e(TAG, "onStart: "+"usuccess")
            }

            override fun onError(requestId: String?, error: ErrorInfo) {
                Log.e(TAG, "onStart: "+error)
            }

            override fun onReschedule(requestId: String?, error: ErrorInfo) {
                Log.e(TAG, "onStart: "+error)
            }
        }).dispatch()
    }
}
