package com.example.cloudstorage

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cloudinary.android.MediaManager
import com.cloudinary.android.callback.ErrorInfo
import com.cloudinary.android.callback.UploadCallback
import com.squareup.picasso.Picasso

import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts


class CloudinaryStorageActivity : AppCompatActivity() {

    private val TAG = "Uploading"

    private val IMAGE_REQ: Int = 1

    private val mImage : AppCompatImageView by lazy {
        findViewById(R.id.imgCldnView)
    }

    private val mUploadBtnCldn : AppCompatButton by lazy {
        findViewById(R.id.btnUploadCldn)
    }

    var imagePath: Uri? = null
    var map: HashMap<String,String> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cloudinary)

        config()

        mImage.setOnClickListener {
            requestPermission()
            Log.e(TAG, ": "+"request permission");
        }

        mUploadBtnCldn.setOnClickListener {
            //uploadToCloudinary(imagePath!!)
            ThreadPool().uploadToCloudinary(imagePath!!)
        }
    }

    private fun config() {
        map["cloud_name"] = "dki63vjgj"
        map["api_key"] = "459771649435237"
        map["api_secret"] = "8T52cz8K4vkpQX6I9KihezBef8o"
        MediaManager.init(this, map)
    }

    private fun requestPermission() {
        if(ContextCompat.checkSelfPermission(this@CloudinaryStorageActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED) {
            accessTheGallery();
        } else {
            ActivityCompat.requestPermissions(this@CloudinaryStorageActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), IMAGE_REQ)
        }
    }

    private fun accessTheGallery() {
        val intent = Intent()
        intent.type = "image/*" // if you want to you can use pdf/gif/video

        intent.action = Intent.ACTION_GET_CONTENT
        startForResult.launch(intent)
    }

    fun uploadToCloudinary(imagePath: Uri) {
        MediaManager.get().upload(imagePath).callback(object : UploadCallback {
            override fun onStart(requestId: String) {
                Log.e(TAG, "onStart: "+"started")
            }

            override fun onProgress(requestId: String, bytes: Long, totalBytes: Long) {
                Log.e(TAG, "onStart: "+"uploading")
            }

            override fun onSuccess(requestId: String, resultData: Map<*, *>) {
                Log.e(TAG, "onStart: "+"usuccess")
            }

            override fun onError(requestId: String?, error: ErrorInfo) {
                Log.e(TAG, "onStart: "+error)
            }

            override fun onReschedule(requestId: String?, error: ErrorInfo) {
                Log.e(TAG, "onStart: "+error)
            }
        }).dispatch()
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        //get the image's file location
//        //get the image's file location
//        filePath = getRealPathFromUri(data?.data!!, this)
//
//        if (requestCode == IMAGE_REQ && resultCode == RESULT_OK) {
//            try {
//                //set picked image to the mProfile
//                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, data?.data)
//                mImage.setImageBitmap(bitmap)
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//        }
//    }

//    private fun getRealPathFromUri(imageUri: Uri, activity: Activity): String? {
//        val cursor: Cursor? = activity.contentResolver.query(imageUri, null, null, null, null)
//
//        return if (cursor == null) {
//            imageUri.path
//        } else {
//            cursor.moveToFirst()
//            val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
//            cursor.getString(idx)
//        }
//    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if(requestCode== IMAGE_REQ){
//            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                accessTheGallery();
//            }else {
//                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            when(result.resultCode){
                RESULT_OK -> {
                    val intent = result.data
                    // Handle the Intent...
                    imagePath = intent?.data
                    Picasso.get().load(imagePath).into(mImage)
                }
            }
        }
}
