package com.example.cloudstorage

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import java.io.IOException
import kotlin.concurrent.thread

class UploadActivity : AppCompatActivity() {

    var sm = FirebaseStorageManager()

    private var filePath: Uri? = null

    private val btnSelectImage: AppCompatButton by lazy {
        findViewById(R.id.select)
    }

    private val btnUploadImage: AppCompatButton by lazy {
        findViewById(R.id.upload)
    }

    private val imgPost: AppCompatImageView by lazy {
        findViewById(R.id.imageView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        init()
    }

    private fun init() {
        btnSelectImage.setOnClickListener {
            imagePicker()
        }

        btnUploadImage.setOnClickListener {
            filePath?.let {
                it1 -> sm.uploadImage(this, it1)
                thread {
                    Thread.sleep(1000)
                    //ParseUploadActivity().uploader(this, it1)
                    //(this@UploadActivity as ParseUploadActivity).uploader(this, it1)
                    ThreadPool().uploader_parse(this, it1)
                }
            }
        }
    }

    private fun imagePicker() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if(data == null || data.data == null) {
                return
            }
            filePath = data.data
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                imgPost.setImageBitmap(bitmap)
            } catch(e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.filemenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.upload_menu -> {
                true
            }
            R.id.show_menu -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}