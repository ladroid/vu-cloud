package com.example.cloudstorage

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText

import android.widget.EditText
import okhttp3.*

import android.widget.TextView
import java.io.IOException
import android.provider.MediaStore

import android.provider.DocumentsContract

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri

import android.os.Environment

import android.os.Build
import android.graphics.Bitmap

import android.graphics.BitmapFactory
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import java.io.ByteArrayOutputStream
import android.widget.Toast

import android.R.attr.data
import android.app.Activity
import android.util.Log
import androidx.appcompat.widget.AppCompatCheckBox


class ServerUploadActivity: AppCompatActivity() {

    private val ipv4AddressView: AppCompatEditText by lazy {
        findViewById(R.id.IPAddress)
    }

    private val portNumberView: AppCompatEditText by lazy {
        findViewById(R.id.portNumber)
    }

    private val responseText: AppCompatTextView by lazy {
        findViewById(R.id.responseText)
    }

    private val isDeletedImg: AppCompatCheckBox by lazy {
        findViewById(R.id.isDeleted)
    }

//    private val btnSelctImage: AppCompatButton by lazy {
//        findViewById(R.id.selectImageServ)
//    }

    private var selectedImagePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_upload)

//        btnSelctImage.setOnClickListener {
//            selectImage()
//        }
    }

    fun connectServer(v: View?) {
        val ipv4Address = ipv4AddressView.text.toString()
        val portNumber = portNumberView.text.toString()

        if(isDeletedImg.isChecked) {
            val postBody = FormBody.Builder().add("delete", "true").build()
            val postUrl = "http://$ipv4Address:$portNumber/"
            postRequest(postUrl, postBody)
        } else {

            Log.e("Upload", "Upload")
            val postUrl = "http://$ipv4Address:$portNumber/"

            val stream = ByteArrayOutputStream()
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.RGB_565
            // Read BitMap by file path
            val bitmap = BitmapFactory.decodeFile(selectedImagePath, options)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            val postBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "image",
                    "androidFlask.jpg",
                    RequestBody.create(MediaType.parse("image/*jpg"), byteArray))
                .build()
            responseText.text = "Please wait..."
            postRequest(postUrl, postBody)
        }
    }

    private fun postRequest(postUrl: String, postBody: RequestBody) {
        val client = OkHttpClient()
        val request = Request.Builder().url(postUrl).post(postBody).build()
        Log.e("request", request.toString() + " " + request.url().toString() + " " + postBody.contentType().toString())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException?) {
                // Cancel the post on failure.
                call.cancel()

                // In order to access the TextView inside the UI thread, the code is executed inside runOnUiThread()
                runOnUiThread {
                    val responseText = findViewById<TextView>(R.id.responseText)
                    responseText.text = "Failed to Connect to Server"
                }
            }

            override fun onResponse(call: Call?, response: Response) {
                // In order to access the TextView inside the UI thread, the code is executed inside runOnUiThread()
                runOnUiThread {
                    val responseText = findViewById<TextView>(R.id.responseText)
                    try {
                        responseText.text = response.body()!!.string()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    fun selectImage(v: View) {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val uri: Uri? = data?.data
            selectedImagePath = getPath(applicationContext, uri!!)
            val imgPath = findViewById<EditText>(R.id.imgPath)
            imgPath.setText(selectedImagePath)
            Toast.makeText(applicationContext, selectedImagePath, Toast.LENGTH_LONG).show()
        }
    }

    // Implementation of the getPath() method and all its requirements is taken from the StackOverflow Paul Burke's answer: https://stackoverflow.com/a/20559175/5426539
    fun getPath(context: Context, uri: Uri): String? {
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.getScheme(), ignoreCase = true)) {
            return getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
            return uri.getPath()
        }
        return null
    }

    fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.getContentResolver().query(
                uri!!, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val column_index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }
}