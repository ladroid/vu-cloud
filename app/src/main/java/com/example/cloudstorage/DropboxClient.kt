package com.example.cloudstorage

import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.DbxRequestConfig

class DropboxClient {
    fun getClient(ACCESS_TOKEN: String?): DbxClientV2? {
        // Create Dropbox client
        val config = DbxRequestConfig.newBuilder("dropbox/sample-app"/*, "en_US"*/).build()
        return DbxClientV2(config, ACCESS_TOKEN)
    }
}