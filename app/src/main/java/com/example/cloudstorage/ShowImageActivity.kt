package com.example.cloudstorage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.app.ProgressDialog
import android.view.View
import android.widget.ProgressBar
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ListResult
import com.google.firebase.storage.StorageReference
import java.util.jar.Manifest


class ShowImageActivity: AppCompatActivity() {

    //recyclerview object
    private val recyclerView: RecyclerView by lazy {
        findViewById(R.id.recyclerView)
    }

    private val progressBar: ProgressBar by lazy {
        findViewById(R.id.progressBar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_image)

        val storage = FirebaseStorage.getInstance()
        val mStorageRef = storage.reference?.child("uploads")
        val imageList: ArrayList<Upload> = ArrayList()
        progressBar.visibility = View.VISIBLE

        val listAllTask: Task<ListResult> = mStorageRef.listAll()
        listAllTask.addOnCompleteListener { result ->
            val items: List<StorageReference> = result.result!!.items
            items.forEachIndexed { index, item ->
                item.downloadUrl.addOnSuccessListener {
                    imageList.add(Upload((it.toString())))
                }.addOnCompleteListener {
                    recyclerView.adapter = DisplayImageViewer(this, imageList)
                    val linearLayoutManager = LinearLayoutManager(this)
                    linearLayoutManager.reverseLayout = true
                    linearLayoutManager.stackFromEnd = true
                    recyclerView.layoutManager = linearLayoutManager
                    progressBar.visibility = View.GONE
                }
            }
        }
    }
}
