package com.example.cloudstorage

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.dropbox.core.v2.DbxClientV2
import java.io.File
import com.dropbox.core.DbxException

import com.dropbox.core.v2.files.WriteMode
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream


class DropboxUploadTask(dbxClient: DbxClientV2?, file: File?, context: Context?): AsyncTask<Any, Any, Any>() {
    private var dbxClient: DbxClientV2? = dbxClient
    private var file: File? = file
    private var context: Context? = context

    override fun doInBackground(params: Array<Any?>?): Any? {
        try {
            // Upload to Dropbox
            //val inputStream: InputStream = FileInputStream(file)
            dbxClient?.files()
                ?.uploadBuilder("/" + file!!.name) //Path in the user's Dropbox to save the file.
                ?.withMode(WriteMode.OVERWRITE) //always overwrite existing file
                ?.uploadAndFinish(FileInputStream(file))
            Log.d("Upload Status", "Success")
        } catch (e: DbxException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    override fun onPostExecute(result: Any?) {
        super.onPostExecute(result)
        Toast.makeText(context, "Image uploaded successfully", Toast.LENGTH_SHORT).show()
    }
}