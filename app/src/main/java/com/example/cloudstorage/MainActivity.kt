package com.example.cloudstorage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton

class MainActivity : AppCompatActivity() {

    private val btnFirebase : AppCompatButton by lazy {
        findViewById(R.id.firebasebtn)
    }

    private val btnBackFApp : AppCompatButton by lazy {
        findViewById(R.id.backapp)
    }

    private val btnCloudinary : AppCompatButton by lazy {
        findViewById(R.id.cloudinary)
    }

    private val btnBoxBtn : AppCompatButton by lazy {
        findViewById(R.id.boxsdkbtn)
    }

    private val btnAzure : AppCompatButton by lazy {
        findViewById(R.id.azurebtnsdk)
    }

    private val btnDropbox : AppCompatButton by lazy {
        findViewById(R.id.dropboxbtnsdk)
    }

    private val btnUploadServer : AppCompatButton by lazy {
        findViewById(R.id.uploadserver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnFirebase.setOnClickListener {
            // Firebase
            val intent = Intent(this@MainActivity, FirebaseActivity::class.java)
            startActivity(intent)
            true
        }

        btnBackFApp.setOnClickListener {
            // Back4App
            val intent = Intent(this@MainActivity, ParseUploadActivity::class.java)
            startActivity(intent)
            true
        }

        btnCloudinary.setOnClickListener {
            // cloudinary
            val intent = Intent(this@MainActivity, CloudinaryStorageActivity::class.java)
            startActivity(intent)
            true
        }

        btnBoxBtn.setOnClickListener {
            // box
            val intent = Intent(this@MainActivity, BoxStorageActivity::class.java)
            startActivity(intent)
            true
        }

        btnAzure.setOnClickListener {
            // azure
            val intent = Intent(this@MainActivity, AzureStorageActivity::class.java)
            startActivity(intent)
            true
        }

        btnDropbox.setOnClickListener {
            // dropbox
            val intent = Intent(this@MainActivity, DropboxActivity::class.java)
            startActivity(intent)
            true
        }

        btnUploadServer.setOnClickListener {
            // upload server
            val intent = Intent(this@MainActivity, ServerUploadActivity::class.java)
            startActivity(intent)
            true
        }
    }
}
