package com.example.cloudstorage

import android.content.Context
import androidx.core.content.ContextCompat.startActivity

import android.content.Intent

import android.content.SharedPreferences

import android.os.Bundle
import android.view.View
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity
import com.dropbox.core.android.Auth


class DropboxLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_dropbox)
        val SignInButton: Button = findViewById<View>(R.id.sign_in_button) as Button
        SignInButton.setOnClickListener {
            Auth.startOAuth2Authentication(applicationContext, getString(R.string.APP_KEY))
        }
    }

    override fun onResume() {
        super.onResume()
        getAccessToken()
    }//Store accessToken in SharedPreferences

    //Proceed to MainActivity
    //generate Access Token
    fun getAccessToken() {
        val accessToken: String = "sl.A9nHodiyf9mE8UAvoLw-yAr41NhfhN6rgFSUxCCB8L0wc2eCIjxGQh8TZl2ynpLqADpP78354saV6MwqckiembBslJQDSdflAp5dVso3mGmiZw5kzl4bmyQoip-OGZxbO1FEp98QJzU" //generate Access Token
        if (accessToken != null) {
            //Store accessToken in SharedPreferences
            val prefs = getSharedPreferences(
                "com.example.cloudstorage",
                Context.MODE_PRIVATE
            )
            prefs.edit().putString("access-token", accessToken).apply()

            //Proceed to MainActivity
            val intent = Intent(this@DropboxLoginActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }
}