package com.example.cloudstorage

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import com.example.cloudstorage.utility.FileHelper
import com.parse.ParseFile
import com.parse.ParseObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

class ParseUploadActivity : AppCompatActivity() {

    private var TAKE_PIC_REQUEST_CODE: Int = 0
    private var CHOOSE_PIC_REQUEST_CODE: Int = 1
    private var MEDIA_TYPE_IMAGE: Int = 2

    private val mAddImageBtn : AppCompatButton by lazy {
        findViewById(R.id.parse_add)
    }

    private val mUploadImageBtn : AppCompatButton by lazy {
        findViewById(R.id.parse_upload)
    }

    private val mPreviewImageView : AppCompatImageView by lazy {
        findViewById(R.id.previewImageView)
    }

    private var mMediaUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parse_main)

        mAddImageBtn.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Upload of a Take Image")
            builder.setPositiveButton("Upload") { dialog: DialogInterface?, whichButton: Int ->
                // Upload Image
                val choosePictureIntent = Intent(Intent.ACTION_GET_CONTENT)
                choosePictureIntent.type = "image/*"
                startActivityForResult(choosePictureIntent, CHOOSE_PIC_REQUEST_CODE)
            }
            builder.setNegativeButton("Take Photo") { dialog: DialogInterface?, whichButton: Int ->
                // Take photo
                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE)
                if(mMediaUri == null) {
                    Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
                } else {
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri)
                    startActivityForResult(takePicture, TAKE_PIC_REQUEST_CODE)
                }
            }
            val dialog = builder.create()
            dialog.show()
        }

        mUploadImageBtn.setOnClickListener {
            ThreadPool().uploader_parse(this, mMediaUri)
            //uploader(this@ParseUploadActivity, mMediaUri)
            thread {
                Thread.sleep(1000)
                val pathMedia = mMediaUri?.path
                (this as CloudinaryStorageActivity).uploadToCloudinary(Uri.parse(pathMedia))
            }
        }
    }

    fun uploader(context: Context, uri: Uri?) {
        val imageUpload = ParseObject("Uploads")
        try {
            var fileBytes = FileHelper().getByteArrayFromFile(context, uri!!)
            if(fileBytes == null) {
                Toast.makeText(applicationContext, "Error!!!", Toast.LENGTH_SHORT).show()
            } else {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                fileBytes = FileHelper().reduceImageForUpload(bitmap)
                val fileName = FileHelper().getFileName(context, uri, "image")
                val file = ParseFile(fileName, fileBytes)
                imageUpload.saveEventually{
                    if(it == null) {
                        Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
                        imageUpload.put("imageContent", file)
                        imageUpload.saveInBackground()
                    } else {
                        Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch(e1: Exception) {
            Toast.makeText(applicationContext, e1.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getOutputMediaFileUri(mediaTypeImage: Int) : Uri?
    {
        if(isExternalStorageAvailable()) {
            val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "UPLOADIMAGES")
            if(!mediaStorageDir.exists()) {
                if(!mediaStorageDir.mkdirs()) {
                    return null
                }
            }
            var mediaFile: File? = null
            val now = Date()
            val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(now)

            val path = mediaStorageDir.path + File.separator
            if(mediaTypeImage == MEDIA_TYPE_IMAGE) {
                mediaFile = File(path + "IMG_" + timestamp + ".jpg")
            }
            return Uri.fromFile(mediaFile)
        } else {
            return null
        }
    }

    private fun isExternalStorageAvailable(): Boolean {
        val state: String = Environment.getExternalStorageState()
        return state == Environment.MEDIA_MOUNTED
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK) {
            if(requestCode == CHOOSE_PIC_REQUEST_CODE) {
                if (data == null) {
                    Toast.makeText(applicationContext, "Image cannot be null", Toast.LENGTH_SHORT).show()
                } else {
                    mMediaUri = data.data
                    mPreviewImageView.setImageURI(mMediaUri)
                }
            } else {
                val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                mediaScanIntent.data = mMediaUri
                sendBroadcast(mediaScanIntent)
            }
        } else if(resultCode != RESULT_CANCELED) {
            Toast.makeText(applicationContext, "Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}
