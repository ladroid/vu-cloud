package com.example.cloudstorage.utility

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.util.Log
import com.google.android.gms.common.util.IOUtils
import java.io.*

class FileHelper {
    public val TAG: String? = FileHelper::class.simpleName
    public val SHORT_SIDE_TARGET: Int = 450

    public fun getByteArrayFromFile(context: Context, uri: Uri) : ByteArray {
        var fileBytes: ByteArray? = null
        var inStream: InputStream?  = null
        var outStream: ByteArrayOutputStream? = null

        if(uri.scheme == "content") {
            try {
                inStream = context.contentResolver.openInputStream(uri)
                outStream = ByteArrayOutputStream()

                val bytesFromFile = ByteArray(1024*1024)
                var bytesRead = inStream?.read(bytesFromFile)
                while(bytesRead != -1) {
                    outStream.write(bytesFromFile, 0, bytesRead!!)
                    bytesRead = inStream?.read(bytesFromFile)
                }
                fileBytes = outStream.toByteArray()
            } catch(e: IOException) {
                Log.e(TAG, e.message!!)
            } finally {
                try {
                    inStream?.close()
                    outStream?.close()
                }catch(e: IOException) {}
            }
        } else {
            try {
                val file: File = File(uri.path)
                val fileInput = FileInputStream(file)
                fileBytes = IOUtils.toByteArray(fileInput)
            } catch(e: IOException) {
                Log.e(TAG, e.message!!)
            }
        }
        return fileBytes!!
    }

    public fun reduceImageForUpload(imageData: Bitmap) : ByteArray {
        val bitmap: Bitmap? = scaleImage(imageData, SHORT_SIDE_TARGET, SHORT_SIDE_TARGET)
        val outputStream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        val reducedData = outputStream.toByteArray()
        try {
            outputStream.close()
        } catch(e: IOException) {}
        return reducedData
    }

    public fun getFileName(context: Context, uri: Uri, fileType: String): String {
        var fileName: String = "image."
        fileName += "png"
        return fileName
    }

    fun scaleImage(bm: Bitmap?, newWidth: Int, newHeight: Int): Bitmap? {
        if (bm == null) {
            return null
        }
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height

        //Keep aspect ratio scaling, mainly long edges
        val scaleRatio = Math.min(scaleHeight, scaleWidth)
        val matrix = Matrix()
        matrix.postScale(scaleRatio, scaleRatio)
        val newBm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true)

        //Create target size bitmap
        val scaledImage = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(scaledImage)

        //Draw background color
        val paint = Paint()
        paint.setColor(Color.GRAY)
        paint.setStyle(Paint.Style.FILL)
        canvas.drawRect(0F, 0F, canvas.getWidth().toFloat(), canvas.getHeight().toFloat(), paint)

        //Determine the screen position
        var left = 0f
        var top = 0f
        if (width > height) {
            top = ((newBm.width - newBm.height) / 2.0).toFloat()
        } else {
            left = ((newBm.height - newBm.width) / 2.0).toFloat()
        }
        canvas.drawBitmap(newBm, left, top, null)
        if (!bm.isRecycled) {
            bm.recycle()
        }
        return scaledImage
    }
}