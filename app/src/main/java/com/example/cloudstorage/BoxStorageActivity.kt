package com.example.cloudstorage

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.box.androidsdk.content.*
import com.box.androidsdk.content.BoxFutureTask.OnCompletedListener
import com.box.androidsdk.content.auth.BoxAuthentication
import com.box.androidsdk.content.auth.BoxAuthentication.AuthListener
import com.box.androidsdk.content.auth.BoxAuthentication.BoxAuthenticationInfo
import com.box.androidsdk.content.models.*
import com.box.androidsdk.content.requests.BoxRequestsFile.UploadFile
import com.box.androidsdk.content.requests.BoxRequestsFile.UploadNewVersion
import com.box.androidsdk.content.requests.BoxResponse
import java.io.IOException
import java.lang.Exception
import java.net.HttpURLConnection


/**
 * Sample content app that demonstrates session creation, and use of file api.
 */
class BoxStorageActivity : AppCompatActivity(), AuthListener {
    var mSession: BoxSession? = null
    var mOldSession: BoxSession? = null
    private var mListView: ListView? = null
    private var mDialog: ProgressDialog? = null
    private var mAdapter: ArrayAdapter<BoxItem>? = null
    private var mFolderApi: BoxApiFolder? = null
    private var mFileApi: BoxApiFile? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_box)
        mListView = findViewById<View>(android.R.id.list) as ListView
        mAdapter = BoxItemAdapter(this)
        mListView!!.adapter = mAdapter
        BoxConfig.IS_LOG_ENABLED = true
        configureClient()
        initSession()
    }

    /**
     * Set required config parameters. Use values from your application settings in the box developer console.
     */
    private fun configureClient() {
        BoxConfig.CLIENT_ID = "4f475e55i9blftbl9qxngb2aisun921t"
        BoxConfig.CLIENT_SECRET = "Qrec8HmuLa8eiLgEsvmhWnio71lCNMLV"

        // needs to match redirect uri in developer settings if set.
        //   BoxConfig.REDIRECT_URL = "<YOUR_REDIRECT_URI>";
    }

    /**
     * Create a BoxSession and authenticate.
     */
    private fun initSession() {
        clearAdapter()
        mSession = BoxSession(this)
        mSession!!.setSessionAuthListener(this)
        mSession!!.authenticate(this)
    }

    override fun onRefreshed(info: BoxAuthenticationInfo) {
        // do nothing when auth info is refreshed
    }

    override fun onAuthCreated(info: BoxAuthenticationInfo) {
        //Init file, and folder apis; and use them to fetch the root folder
        mFolderApi = BoxApiFolder(mSession)
        mFileApi = BoxApiFile(mSession)
        loadRootFolder()
    }

    override fun onAuthFailure(info: BoxAuthenticationInfo, ex: Exception) {
        if (ex != null) {
            clearAdapter()
        } else if (info == null && mOldSession != null) {
            mSession = mOldSession
            mSession!!.setSessionAuthListener(this)
            mOldSession = null
            onAuthCreated(mSession!!.authInfo)
        }
    }

    override fun onLoggedOut(info: BoxAuthenticationInfo, ex: Exception) {
        clearAdapter()
        initSession()
    }

    //Method to demonstrate fetching folder items from the root folder
    private fun loadRootFolder() {
        object : Thread() {
            override fun run() {
                try {
                    //Api to fetch root folder
                    val folderItems =
                        mFolderApi!!.getItemsRequest(BoxConstants.ROOT_FOLDER_ID).send()
                    runOnUiThread {
                        mAdapter!!.clear()
                        for (boxItem in folderItems) {
                            mAdapter!!.add(boxItem)
                        }
                    }
                } catch (e: BoxException) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    /**
     * Method demonstrates a sample file being uploaded using the file api
     */
    private fun uploadSampleFile() {
        mDialog = ProgressDialog.show(
            this@BoxStorageActivity,
            getText(R.string.boxsdk_Please_wait),
            getText(R.string.boxsdk_Please_wait)
        )
        object : Thread() {
            override fun run() {
                try {
                    val uploadFileName = "box_logo.png"
                    val uploadStream = resources.assets.open(uploadFileName)
                    val destinationFolderId = "0"
                    val uploadName = "BoxSDKUpload.png"
                    val request =
                        mFileApi!!.getUploadRequest(uploadStream, uploadName, destinationFolderId)
                    val uploadFileInfo = request.send()
                    showToast("Uploaded " + uploadFileInfo.name)
                    loadRootFolder()
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: BoxException) {
                    e.printStackTrace()
                    val error = e.asBoxError
                    if (error != null && error.status == HttpURLConnection.HTTP_CONFLICT) {
                        val conflicts = error.contextInfo.conflicts
                        if (conflicts != null && conflicts.size == 1 && conflicts[0] is BoxFile) {
                            uploadNewVersion(conflicts[0] as BoxFile)
                            return
                        }
                    }
                    showToast("Upload failed")
                } finally {
                    mDialog?.dismiss()
                }
            }
        }.start()
    }

    /**
     * Method demonstrates a new version of a file being uploaded using the file api
     * @param file
     */
    private fun uploadNewVersion(file: BoxFile) {
        object : Thread() {
            override fun run() {
                try {
                    val uploadFileName = "download.jpeg"
                    val uploadStream = resources.assets.open(uploadFileName)
                    val request = mFileApi!!.getUploadNewVersionRequest(uploadStream, file.id)
                    val uploadFileVersionInfo = request.send()
                    showToast("Uploaded new version of " + uploadFileVersionInfo.name)
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: BoxException) {
                    e.printStackTrace()
                    showToast("Upload failed")
                } finally {
                    mDialog!!.dismiss()
                }
            }
        }.start()
    }

    private fun showToast(text: String) {
        runOnUiThread { Toast.makeText(this@BoxStorageActivity, text, Toast.LENGTH_LONG).show() }
    }

    private fun clearAdapter() {
        runOnUiThread { mAdapter!!.clear() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val numAccounts = BoxAuthentication.getInstance().getStoredAuthInfo(this).keys.size
        menu.findItem(R.id.logoutAll).isVisible = numAccounts > 1
        menu.findItem(R.id.logout).isVisible = numAccounts > 0
        menu.findItem(R.id.switch_accounts)
            .setTitle(if (numAccounts > 0) R.string.switch_accounts else R.string.login)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.upload) {
            uploadSampleFile()
            return true
        } else if (id == R.id.switch_accounts) {
            switchAccounts()
            return true
        } else if (id == R.id.logout) {
            mSession!!.logout()
            return true
        } else if (id == R.id.logoutAll) {
            object : Thread() {
                override fun run() {
                    BoxAuthentication.getInstance().logoutAllUsers(applicationContext)
                }
            }.start()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun switchAccounts() {
        mOldSession = mSession
        // when switching accounts we don't care about events for the old session.
        mOldSession!!.setSessionAuthListener(null)
        mSession = BoxSession(this, null)
        mSession!!.setSessionAuthListener(this)
        mSession!!.authenticate(this).addOnCompletedListener { response ->
            if (response.isSuccess) {
                clearAdapter()
            }
        }
    }

    private inner class BoxItemAdapter(context: Context?) :
        ArrayAdapter<BoxItem>(context!!, 0) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            val item = getItem(position)
            if (convertView == null) {
                convertView =
                    LayoutInflater.from(context).inflate(R.layout.boxsdk_list_item, parent, false)
            }
            val name = convertView!!.findViewById<View>(R.id.name) as TextView
            name.text = item!!.name
            val icon = convertView.findViewById<View>(R.id.icon) as ImageView
            if (item is BoxFolder) {
                icon.setImageResource(R.drawable.box_circle_mask)
            } else {
                icon.setImageResource(R.drawable.boxlogo_white)
            }
            return convertView
        }
    }
}