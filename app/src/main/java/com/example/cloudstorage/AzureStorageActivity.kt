package com.example.cloudstorage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.graphics.Bitmap
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import android.provider.MediaStore

import android.content.Intent
import com.microsoft.azure.storage.CloudStorageAccount
import android.content.Context
import android.database.Cursor
import android.net.Uri
import java.io.*
import java.lang.Exception
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy


class AzureStorageActivity: AppCompatActivity() {

    private val btnUpload : AppCompatButton by lazy {
        findViewById(R.id.btnUploadAzure)
    }

    private val btnChoose : AppCompatButton by lazy {
        findViewById(R.id.btnChooseAzure)
    }

    private val btnCapture : AppCompatButton by lazy {
        findViewById(R.id.btnCaptureAzure)
    }

    private val imgView : AppCompatImageView by lazy {
        findViewById(R.id.imgViewAzure)
    }

    private var inputStream: ByteArrayInputStream? = null

    private var currImageURI: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_azure)

        btnChoose.setOnClickListener {
            chooseImage()
        }

        btnCapture.setOnClickListener {
            //captureImage()
        }

        btnUpload.setOnClickListener {
            //uploadImage()
        }
    }

    fun chooseImage() {
        // To open up a gallery browser
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

                // currImageURI is the global variable I'm using to hold the content:// URI of the image
                currImageURI = data?.data

                imgView.setImageURI(currImageURI)

                val policy = ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)

                //Here starts the code for Azure Storage Blob
                try {
                    // Retrieve storage account from connection-string.
                    val storageAccount = CloudStorageAccount.parse("DefaultEndpointsProtocol=https;AccountName=vucloud;AccountKey=obIaEzKMmUhP9E66162qLo07ARHDiDvtBkk0MitMPg3srUhRHR4+JgQxStJ2EwYXbWE1lLjlQ2ulB4W+ONXylA==;EndpointSuffix=core.windows.net")

                    // Create the blob client.
                    val blobClient = storageAccount.createCloudBlobClient()

                    // Retrieve reference to a previously created container.
                    val container = blobClient.getContainerReference("images")
                    
                    container.createIfNotExists()

                    // Create or overwrite the blob (with the name "example.jpeg") with contents from a local file.
                    val blob = container.getBlockBlobReference("${getRealPathFromURI(this, currImageURI)}.jpg")
                    val source = File(getRealPathFromURI(this, currImageURI))
                    blob.upload(FileInputStream(source), source.length())
                } catch (e: Exception) {
                    // Output the stack trace.
                    e.printStackTrace()
                }
            }
        }
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri?): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.getContentResolver().query(contentUri!!, proj, null, null, null)
            val column_index: Int = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }
}
