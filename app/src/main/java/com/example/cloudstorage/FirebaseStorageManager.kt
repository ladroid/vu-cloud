package com.example.cloudstorage

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.util.*

class FirebaseStorageManager {
    private val mStorageRef = FirebaseStorage.getInstance().reference

    fun uploadImage(context: Context, imageFileUri: Uri) {
        if(imageFileUri != null) {
            val ref = mStorageRef?.child("uploads/" + UUID.randomUUID().toString())
            ref?.putFile(imageFileUri!!)?.addOnSuccessListener {
                Toast.makeText(context, "Image Uploaded", Toast.LENGTH_SHORT).show()
            }?.addOnFailureListener {
                Toast.makeText(context, "Image Uploading Failed", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(context, "Please select an Image", Toast.LENGTH_SHORT).show()
        }
    }
}