package com.example.cloudstorage

import android.R.attr
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.android.Auth
import android.content.SharedPreferences
import android.util.Log
import com.google.firebase.storage.UploadTask

import android.R.attr.data
import android.app.Activity
import android.widget.Toolbar
import com.google.android.gms.auth.api.phone.SmsRetriever.getClient
import com.google.android.gms.safetynet.SafetyNet.getClient
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.io.File
import com.dropbox.core.v2.users.FullAccount
import java.lang.Exception
import com.squareup.picasso.Picasso

import android.view.View
import android.widget.ImageView

import android.widget.TextView


class DropboxActivity : AppCompatActivity() {

    private val IMAGE_REQUEST_CODE = 101
    private var ACCESS_TOKEN: String? = null

    private val mToolbar: Toolbar by lazy {
        findViewById(R.id.toolbar)
    }

    private val mFloatingActionButton: FloatingActionButton by lazy {
        findViewById(R.id.fab)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dropbox)
        if(!tokenExists()) {
            val intent = Intent(this, DropboxLoginActivity::class.java)
            startActivity(intent)
        }

        ACCESS_TOKEN = retrieveAccessToken()
        getUserAccount()

        mFloatingActionButton.setOnClickListener {
            upload()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode !== RESULT_OK || data == null) return
        // Check which request we're responding to
        // Check which request we're responding to
        if (requestCode === IMAGE_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode === RESULT_OK) {
                //Image URI received
                val file = File(data.data?.let { URI_to_Path().getPath(this, it) })
                    //val file = data.data
                DropboxUploadTask(
                    DropboxClient().getClient(ACCESS_TOKEN),
                    file,
                    applicationContext
                ).execute()
            }
        }
    }

    protected fun getUserAccount() {
        if (ACCESS_TOKEN == null) return
        DropboxClient().getClient(ACCESS_TOKEN)?.let {
            DropboxUserAccountTask(it, object : DropboxUserAccountTask.TaskDelegate {
                override fun onAccountReceived(account: FullAccount?) {
                    Log.d("User data", account!!.email)
                    Log.d("User data", account.name.displayName)
                    Log.d("User data", account.accountType.name)
                    updateUI(account)
                }

                override fun onError(error: Exception?) {
                    Log.d("User data", "Error receiving account details.")
                }
            }).execute()
        }
    }

    fun upload() {
        if (ACCESS_TOKEN == null) return
        //Select image to upload
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        startActivityForResult(
            Intent.createChooser(
                intent,
                "Upload to Dropbox"
            ), IMAGE_REQUEST_CODE
        )
        Log.e("Upload", "Upload")
    }

    private fun tokenExists(): Boolean {
        val prefs = getSharedPreferences("com.example.cloudstorage", Context.MODE_PRIVATE)
        val accessToken = prefs.getString("access-token", null)
        return accessToken != null
    }

    private fun retrieveAccessToken(): String? {
        //check if ACCESS_TOKEN is previously stored on previous app launches
        val prefs = getSharedPreferences("com.example.cloudstorage", MODE_PRIVATE)
        val accessToken = prefs.getString("access-token", null)
        return if (accessToken == null) {
            Log.d("AccessToken Status", "No token found")
            null
        } else {
            //accessToken already exists
            Log.d("AccessToken Status", "Token exists")
            accessToken
        }
    }

    fun updateUI(account: FullAccount) {
        val profile: ImageView = findViewById<View>(R.id.dropbox_imageView) as ImageView
        val name = findViewById<View>(R.id.dropbox_name_textView) as TextView
        val email = findViewById<View>(R.id.dropbox_email_textView) as TextView

        name.setText(account.getName().getDisplayName())
        email.setText(account.getEmail())
        Picasso.get()
            .load(account.getProfilePhotoUrl())
            .resize(200, 200)
            .into(profile)
    }
}